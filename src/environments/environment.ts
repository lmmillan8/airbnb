// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyB3nh_VzZ9u6_hUe2guCiiARCAjYslQoJk',
    authDomain: 'airbnb-c6834.firebaseapp.com',
    projectId: 'airbnb-c6834',
    storageBucket: 'airbnb-c6834.appspot.com',
    messagingSenderId: '250412568036',
    appId: '1:250412568036:web:7b4c56832fdc207bf20bf1',
    measurementId: 'G-DT0H5ZVX2K'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
