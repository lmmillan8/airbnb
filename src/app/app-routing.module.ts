import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './principal/main/main.component';


const routes: Routes = [

  {
    path: 'hosting',
    loadChildren: () => import ('./hosting/hosting.module')
    .then(m => m.HostingModule)
  },
  {
    path: 'principal',
    loadChildren: () => import('./principal/principal.module')
    .then(m => m.PrincipalModule)
  },
  {
    path: 'main',
    component: MainComponent
  },

  {
    path: 'activities',
    loadChildren: () => import('./activities/activities.module')
    .then(m => m.ActivitiesModule)
  },
  {
    path: 'places',
    loadChildren: () => import('./places/places.module')
    .then(m => m.PlacesModule)
  },
  {
    path: 'images',
    loadChildren: () => import('./images/images.module')
    .then(m => m.ImagesModule)
  },

  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
