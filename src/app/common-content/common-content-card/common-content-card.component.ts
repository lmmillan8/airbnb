import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-common-content-card',
  templateUrl: './common-content-card.component.html',
  styleUrls: ['./common-content-card.component.styl']
})
export class CommonContentCardComponent implements OnInit {
  @Input() item: any;
  @Input() places: any;
  @Input() imagesPlaces: any;

  constructor() { }

  ngOnInit(): void {
  }

}
