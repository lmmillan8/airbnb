import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonContentCardComponent } from './common-content-card.component';

describe('CommonContentCardComponent', () => {
  let component: CommonContentCardComponent;
  let fixture: ComponentFixture<CommonContentCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonContentCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonContentCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
