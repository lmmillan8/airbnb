import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonContentCardComponent } from './common-content-card/common-content-card.component';



@NgModule({
  declarations: [CommonContentCardComponent],
  imports: [
    CommonModule
  ],
  exports:[
    CommonContentCardComponent
  ]
})
export class CommonContentModule { }
