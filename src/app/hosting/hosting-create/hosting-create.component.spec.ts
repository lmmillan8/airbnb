import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HostingCreateComponent } from './hosting-create.component';

describe('HostingCreateComponent', () => {
  let component: HostingCreateComponent;
  let fixture: ComponentFixture<HostingCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HostingCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostingCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
