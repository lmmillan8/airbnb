import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HostingCreateComponent } from './hosting-create/hosting-create.component';
import { HostingListComponent } from './hosting-list/hosting-list.component';
import { HostingViewComponent } from './hosting-view/hosting-view.component';


const routes: Routes = [
  {
    path: 'hosting-create',
    component: HostingCreateComponent
  },
  {
    path: 'hosting-list',
    component: HostingListComponent
  },
  {
    path: 'hosting-view',
    component: HostingViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HostingRoutingModule { }
