import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class HostingService {

  constructor(private firebase: AngularFirestore) { }
  getHosting() {
    return this.firebase.collection('hosting').snapshotChanges();
  }
}
