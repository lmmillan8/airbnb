import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HostingRoutingModule } from './hosting-routing.module';
import { HostingListComponent } from './hosting-list/hosting-list.component';
import { HostingCreateComponent } from './hosting-create/hosting-create.component';
import { HostingViewComponent } from './hosting-view/hosting-view.component';
import { CommonContentModule } from '../common-content/common-content.module';


@NgModule({
  declarations: [HostingListComponent, HostingCreateComponent, HostingViewComponent],
  imports: [
    CommonModule,
    HostingRoutingModule,
    CommonContentModule
  ],
  exports:[
    HostingListComponent,
    HostingCreateComponent,
    HostingViewComponent
  ]
})
export class HostingModule { }
