import { Component, OnInit } from '@angular/core';
import { IHosting } from '../hosting.model';
import { HostingService } from '../hosting.service';

@Component({
  selector: 'app-hosting-list',
  templateUrl: './hosting-list.component.html',
  styleUrls: ['./hosting-list.component.styl']
})
export class HostingListComponent implements OnInit {

  listHosting: IHosting[];
  collection = { count: 0, data: [] };
  config: any;

  constructor(private service: HostingService) { }

  ngOnInit(): void {
    this.service.getHosting().subscribe(res => {
      this.collection.data = res.map((e: any) => {
        return {
          id: e.payload.doc.id,
          idHosting: e.payload.doc.data().id,
          name: e.payload.doc.data().name,
          image: e.payload.doc.data().image
        };
      });
      console.warn('res', this.collection.data);
      this.listHosting = this.collection.data;
    });
  }
}


