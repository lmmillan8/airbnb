import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrincipalRoutingModule } from './principal-routing.module';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HostingModule } from '../hosting/hosting.module';
import { AppModule } from '../app.module';
import { LoginComponent } from './login/login.component';
import { PageComponent } from './page/page.component';
import { PlacesModule } from '../places/places.module';
import { ActivitiesModule } from '../activities/activities.module';


@NgModule({
  declarations: [FooterComponent, HeaderComponent, MainComponent, NavbarComponent, LoginComponent, PageComponent],
  imports: [
    CommonModule,
    PrincipalRoutingModule,
    AppModule,
    HostingModule,
    PlacesModule,
    ActivitiesModule
  ]
})
export class PrincipalModule {}

