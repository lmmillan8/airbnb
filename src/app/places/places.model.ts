export interface IPlace {
  id?: string;
  idPlace: number;
  name: string;
  description: string;
  image: string;
}

export class Places implements IPlace {
  id?: string;
  idPlace: number;
  name: string;
  image: string;
  description: string;
}
