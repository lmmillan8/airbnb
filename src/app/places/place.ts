export interface IPlace {
    id?: string;
    idPlace: number;
    name?: string;
    image?: string;

}

export class Place implements IPlace{
    constructor(
    public id: string,
    public idPlace: number,
    public name: string,
    public image: string
    ){}
}