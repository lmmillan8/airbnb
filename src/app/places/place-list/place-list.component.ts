import { Component, OnInit } from '@angular/core';
import { PlaceService } from '../place.service';
import { IPlace } from '../place';

@Component({
  selector: 'app-place-list',
  templateUrl: './place-list.component.html',
  styleUrls: ['./place-list.component.styl']
})
export class PlaceListComponent implements OnInit {

  imagesPlaces = true;
  places = true;

  listPlaces: IPlace[];
  collection = { count: 0, data: [] };
  config: any;

  constructor(private service: PlaceService) { }

  ngOnInit(): void {
    this.service.getPlace().subscribe(res => {
      this.collection.data = res.map((e: any) => {
        return {
          id: e.payload.doc.id,
          idPlace: e.payload.doc.data().id,
          name: e.payload.doc.data().name,
          description: e.payload.doc.data().description,
          image: e.payload.doc.data().image
        };
      });
      console.warn('res', this.collection.data);
      this.listPlaces = this.collection.data;
    });
  }

}
