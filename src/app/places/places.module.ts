import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlacesRoutingModule } from './places-routing.module';
import { PlaceListComponent } from './place-list/place-list.component';
import { CommonContentModule } from '../common-content/common-content.module';


@NgModule({
  declarations: [PlaceListComponent],
  imports: [
    CommonModule,
    PlacesRoutingModule,
    CommonContentModule
  ],
  exports: [
    PlaceListComponent
  ]
})
export class PlacesModule { }
