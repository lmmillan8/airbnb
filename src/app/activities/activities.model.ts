export interface IActivities {
  id?: string;
  idACtivities: number;
  name: string;
  description: string;
  image: string;
}

export class Activities implements IActivities {
  id?: string;
  idACtivities: number;
  name: string;
  image: string;
  description: string;
}