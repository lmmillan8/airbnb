import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivitiesRoutingModule } from './activities-routing.module';
import { ActivitiesListComponent } from './activities-list/activities-list.component';
import { CommonContentModule } from '../common-content/common-content.module';


@NgModule({
  declarations: [ActivitiesListComponent],
  imports: [
    CommonModule,
    ActivitiesRoutingModule,
    CommonContentModule
  ],
  exports: [
    ActivitiesListComponent
  ]
})
export class ActivitiesModule { }
