import { Component, OnInit } from '@angular/core';
import { IActivities } from '../activities.model';
import { ActivitiesService } from '../activities.service';

@Component({
  selector: 'app-activities-list',
  templateUrl: './activities-list.component.html',
  styleUrls: ['./activities-list.component.styl']
})
export class ActivitiesListComponent implements OnInit {

  listActivities: IActivities[];
  collection = { count: 0, data: [] };
  config: any;

  constructor(private service: ActivitiesService) { }

  ngOnInit(): void {
    this.service.getActivities().subscribe(res => {
      this.collection.data = res.map((e: any) => {
        return {
          id: e.payload.doc.id,
          idActivities: e.payload.doc.data().id,
          name: e.payload.doc.data().name,
          description: e.payload.doc.data().description,
          image: e.payload.doc.data().image
        };
      });
      console.warn('res', this.collection.data);
      this.listActivities = this.collection.data;
    });
  }
}


