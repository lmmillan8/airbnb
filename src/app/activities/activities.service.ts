import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ActivitiesService {

  constructor(private firebase: AngularFirestore) { }
  getActivities() {
    return this.firebase.collection('activities').snapshotChanges();
  }
}
